const login = require("../db_apis/login.js");
const jwt = require("jsonwebtoken");
function getUserFromRec(req) {
  const loginData = {
    email: req.body.email,
    password: req.body.password,
  };

  return loginData;
}

async function post(req, res, next) {
  try {
    let loginData = getUserFromRec(req);
    loginData = await login.validate(loginData);
    if (loginData.length === 1) {
      console.log("loginData:", loginData[0]);
      let token = jwt.sign(loginData[0], "secretvalue", {
        expiresIn: "1440m", // Token time changing it to 24hrs for NBAA - 38699
      });
      res.status(200).json({
        message: "Login successful",
        token: token,
      });
      //   res.status(200).json(loginData);
    } else {
      res.status(401).json();
    }
  } catch (err) {
    next(err);
  }
}

module.exports.post = post;
