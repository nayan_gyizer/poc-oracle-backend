const users = require("../db_apis/users.js");
const jwt = require("jsonwebtoken");

async function get(req, res, next) {
  try {
    const context = {};
    if (!req.headers || !req.headers.authorization) {
      return res.status(401).end();
    }

    jwt.verify(
      req.headers.authorization,
      "secretvalue",
      async function (err, jwtRes) {
        if (err) {
          console.log(err);
          return res.status(401).end();
        } else {
          context.id = parseInt(req.params.id, 10);

          const rows = await users.find(context);

          if (req.params.id) {
            if (rows.length === 1) {
              res.status(200).json(rows[0]);
            } else {
              res.status(404).end();
            }
          } else {
            res.status(200).json(rows);
          }
        }
      }
    );
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;

async function getUserFromRec(req) {
  const user = {
    username: req.body.username,
    email: req.body.email,
    mobile: req.body.mobile,
    password: req.body.password,
  };

  return user;
}

async function post(req, res, next) {
  try {
    let user = await getUserFromRec(req);
    user = await users.create(user);
    res.status(201).json(user);
  } catch (err) {
    next(err);
  }
}

module.exports.post = post;

async function put(req, res, next) {
  try {
    let user = getUserFromRec(req);
    user.id = parseInt(req.params.id, 10);
    user = await users.update(user);
    res.status(201).json(user);

    if (user !== null) {
      res.status(200).json(user);
    } else {
      res.status(404).end();
    }
  } catch (err) {
    next(err);
  }
}

module.exports.put = put;

async function del(req, res, next) {
  try {
    const id = parseInt(req.params.id, 10);

    const success = await users.delete(id);

    if (success) {
      res.status(204).end();
    } else {
      res.status(404).end();
    }
  } catch (err) {
    next(err);
  }
}

module.exports.delete = del;
