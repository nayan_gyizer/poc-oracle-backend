const express = require("express");
const router = new express.Router();
const users = require("../controllers/users.js");
const login = require("../controllers/login.js");
router
  .route("/users/:id?")
  .get(users.get)
  .post(users.post)
  .put(users.put)
  .delete(users.delete);

router.route("/login").post(login.post);

module.exports = router;
