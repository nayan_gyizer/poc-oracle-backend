const oracledb = require("oracledb");
const database = require("../services/database.js");

const baseQuery = `select ID, USERNAME, MOBILE, EMAIL from APP_USERS`;

async function find(context) {
  let query = baseQuery;
  const binds = {};

  if (context.id) {
    binds.id = context.id;

    query += `\nwhere id = :id`;
  }

  const result = await database.simpleExecute(query, binds);

  return result.rows;
}

module.exports.find = find;

const createSql = `insert into app_users (username, email, mobile, password) values (:username, :email, :mobile, :password) returning id into :id`;

async function create(usr) {
  const user = Object.assign({}, usr);

  user.id = {
    dir: oracledb.BIND_OUT,
    type: oracledb.NUMBER,
  };

  const result = await database.simpleExecute(createSql, user, {
    autoCommit: true,
  });
  user.id = result.outBinds.id[0];
  return user;
}

module.exports.create = create;

const updateSql = `update app_users
  set username = :username,
    email = :email,
    mobile = :mobile,
    password = :password
  where id = :id`;

async function update(usr) {
  const user = Object.assign({}, usr);
  console.log(user);
  const result = await database.simpleExecute(updateSql, user, {
    autoCommit: true,
  });

  if (result.rowsAffected && result.rowsAffected === 1) {
    return user;
  } else {
    return null;
  }
}

module.exports.update = update;

const deleteSql = `begin
    delete from app_users
    where id = :id;
    :rowcount := sql%rowcount;
  end;`;

async function del(id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER,
    },
  };
  const result = await database.simpleExecute(deleteSql, binds, {
    autoCommit: true,
  });

  return result.outBinds.rowcount === 1;
}

module.exports.delete = del;
