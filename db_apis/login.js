const oracledb = require("oracledb");
const database = require("../services/database.js");
const bcrypt = require("bcrypt");

const baseQuery = `select ID, USERNAME, MOBILE, EMAIL from APP_USERS where email= :email and password= :password`;

async function find(context) {
  let query = baseQuery;
  const binds = {};
  binds.email = context.email;
  binds.password = context.password;
  const result = await database.simpleExecute(query, binds);
  return result.rows;
}

async function validate(context) {
  const row = await find(context);
  return row;
}

module.exports.validate = validate;
